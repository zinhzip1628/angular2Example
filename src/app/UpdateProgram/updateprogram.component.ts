import {Component, OnInit, Input, Output} from '@angular/core';
import { ListProgramServices } from '../ListProgram/listprogram.services';
import { ActivatedRoute, Router } from '@angular/router';
import { ILiveProgramList } from '../ListProgram/ILiveProgramList';
import { EventEmitter } from 'events';

@Component({
    selector: 'app-updateprogram',
    templateUrl: '/updateprogram.component.html',
})
export class UpdateProgramComponent implements OnInit {
    LiveProgram: ILiveProgramList[];
    statusMessage = 'Loading data please wait ...';
    constructor ( private _ListProgramService: ListProgramServices, private _activatedRouter: ActivatedRoute,
    private _route: Router) {
    }
    ngOnInit(): void {
        const empID = this._activatedRouter.snapshot.params['ID'];
        this._ListProgramService.getLivePrograms().subscribe((liveprogramdata) => this.LiveProgram = liveprogramdata,
        (error => {
            this.statusMessage = 'Problem with the services. Please try again ...!';
        }));
    }
    OnClickLiveProgram(value: number) {
        console.log(value);
        this._route.navigate(['/updateprogram/' + value]);
    }
}

export interface IGameMoKet {
    Date: string;
    ID: number;
    ImageInvitee: string;
    Invitee: string;
    MC: string;
    MC2: string;
    Manager: string;
    Name: string;
    Place: string;
    Questions: string;
    TVCDate: string;
    TVCLink: string;
    User: string;
    Director: string;
}

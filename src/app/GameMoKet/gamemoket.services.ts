import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { IGameMoKet } from './IGamemoket';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/Observable/throw';

@Injectable()
export class GameMoketService {
    constructor(private _http: Http) {}

    getGameMoKets(): Observable<IGameMoKet[]> {
        return this._http.get('http://localhost:54208/api/ContentGameMoKet/IN_GameMK_GetInfoGameMK?type=0&idinfo=0')
        .map((response: Response) => <IGameMoKet[]>response.json())
        .catch(this.handleError);
    }
    getGameMoKetById(empID: number): Observable<IGameMoKet[]> {
        return this._http.get('http://localhost:54208/api/ContentGameMoKet/IN_GameMK_GetInfoGameMK?type=1&idinfo=' + empID)
        .map((response: Response) => <IGameMoKet[]>response.json())
        .catch(this.handleError);
    }
    handleError(error: Response) {
        // console.error(error);
        return Observable.throw(error);
    }
}

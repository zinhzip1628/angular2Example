import {Component, OnInit} from '@angular/core';
import { IGameMoKet } from './IGamemoket';
import { GameMoketService } from './gamemoket.services';
import { ColourService } from '../Colour/colour.service';

@Component({
    selector: 'app-gamemoket',
    templateUrl: '/gamemoket.component.html',
    providers: [GameMoketService]
    })

export class GameMoKetComponent implements OnInit {

    gamemokets: IGameMoKet[];
    statusMessage = 'Loading data please wait ...';
    field1 = 'ID';
    field2 = 'Name';
    field3 = 'ID';
    private colourservice: ColourService;
    private _colourservice: ColourService;
    constructor(private _gamemoketservice: GameMoketService) {
        this._colourservice = new ColourService();
    }
    // constructor(private _gamemoketservice: GameMoketService, private _colourservice: ColourService) {
    //     this.colourservice = _colourservice;
    // }
    getgamemoket(): void {
        this._gamemoketservice.getGameMoKets().subscribe((gamemoketdata) => this.gamemokets = gamemoketdata,
    (error => {
        this.statusMessage = 'Problem with the service. Please try again !!!';
        // console.error(error);
    }));
    }
    ngOnInit(): void {
        this._gamemoketservice.getGameMoKets().subscribe((gamemoketdata) => this.gamemokets = gamemoketdata,
    (error => {
        this.statusMessage = 'Problem with the service. Please try again !!!';
        // console.error(error);
    }));
    }
    get colour(): string {
        return this._colourservice.colourPreference;
    }
    set colour(value: string) {
         this._colourservice.colourPreference = value;
    }
}

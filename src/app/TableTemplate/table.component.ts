import {Component, Input, Output} from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent {
  @Input() items;
  @Input() field1;
  @Input() field2;
  @Input() field3;
  @Input() field4;

  constructor() {
  }
  trackByFn(index: number, items: any): string {
    return items.field1;
  }
}

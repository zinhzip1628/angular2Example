import { Component } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

}

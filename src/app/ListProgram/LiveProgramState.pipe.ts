import {Pipe, PipeTransform, OnInit} from '@angular/core';
import {ILiveProgramList} from './ILiveProgramList';

@Pipe({
    name: 'LiveStatePipe'
})
export class LiveProgramState implements PipeTransform {

    str = '';
    transform(value: ILiveProgramList, LiveProgram: ILiveProgramList): string {
        this.str = '';
        if (!LiveProgram) {
                return '';
        }
        if (LiveProgram.ProgamStatus === 1) {// hoan tất chương trình
            return '<span class="label label-info label-mini">CT đã hoàn tất</span><br/>';
        }
        if (LiveProgram.ProgamStatus === 2) { // hủy chương trinh
            return '<span class="label label-danger label-mini" title="Nguyên Nhân:  '
            + LiveProgram.CancelReasonProgram + '">CT đã hủy</span><br/>';
        }
        // Check chuong trinh duyet hay chua
        if (LiveProgram.Approve === 0) {
            this.str += '<span class="label label-danger label-mini">CT chưa duyệt</span><br/>';
        } else if (LiveProgram.Approve === 1) {
            this.str += '<span class="label label-info label-mini">CT đã duyệt</span><br/>';
        } else if (LiveProgram.Approve === 2) {
            this.str += '<span class="label label-danger label-mini">CT không được duyệt</span><br/>';
        } else if (LiveProgram.Approve === 3) {
            this.str += '<span class="label label-default label-mini" title="Người send mail: '
            + LiveProgram.ApproveUser + '">CT đã gửi mail</span><br/>';
        }
        // check phieu khao sat
        if (LiveProgram.ResultSurvey === 0) {
            this.str += '<span class="label label-danger label-mini">Khảo sát chưa duyệt</span><br/>';
        } else {
            if (LiveProgram.ReasonSurvey1 == null && LiveProgram.ReasonSurvey2 == null) {
                this.str += '<span class="label label-info label-mini">Phiếu y/c ks đã gửi</span><br/>';
            } else {
                this.str += '<span class="label label-default label-mini">PKS - FTI '
                 + LiveProgram.ReasonSurvey1 + '</span><br/>' + '<span class="label label-default label-mini">PKS - FTEL '
                 + LiveProgram.ReasonSurvey2 + '</span><br/>' + '<span class="label label-default label-mini">PTC - FTI '
                 + LiveProgram.ReasonConstruction1 + '</span><br/>' + '<span class="label label-default label-mini">PTC - FTEL '
                 + LiveProgram.ReasonConstruction2 + '</span><br/>'
                 + '<span class="label label-default label-mini">Ticket ' + LiveProgram.ReasonTicketStatus + '</span><br/>';
            }
        }

        // check nghiem thu
        if (LiveProgram.Accept === 0) {
            this.str += '<span class="label label-danger label-mini">Chưa nghiệm thu</span><br/>';
        } else if (LiveProgram.Accept === 2) {
            this.str += '<span class="label label-danger label-mini;">Không nghiệm thu</span><br/>';
        } else {
            this.str += '<span class="label label - info label - mini; ">Đã nghiệm thu</span><br/>';
        }
        return this.str;
    }
}

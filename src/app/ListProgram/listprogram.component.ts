import { Component, OnInit } from '@angular/core';
import { ILiveProgramList } from './ILiveProgramList';
import { ListProgramServices } from './listprogram.services';
import { ColourService } from '../Colour/colour.service';

@Component({
    selector: 'app-list',
    templateUrl: '/listprogram.component.html'
})
export class ListProgramComponent implements OnInit {
    liveprograms: ILiveProgramList[];
    statusMessage = 'Loading data please wait ...';
    url = 'updateprogram';
    field1 = 'ProgamID';
    field2 = 'Title';
    field3 = 'id';
    field4 = 'url';
    private _colourservice: ColourService;
    private colourservice: ColourService;
    constructor(private _liveprogramservice: ListProgramServices) {
        this._colourservice = new ColourService();
    }
    // constructor(private _liveprogramservice: ListProgramServices, private _colourservice: ColourService) {
    //     this.colourservice = _colourservice;
    //     }
    ngOnInit(): void {
        this._liveprogramservice.getLivePrograms().subscribe((liveprogramdata) => this.liveprograms = liveprogramdata,
    (error => {
        this.statusMessage = 'Problem with the services. Please try again ...!';
    }));
    }
    get colour(): string {
        return this._colourservice.colourPreference;
    }
    set colour(value: string) {
         this._colourservice.colourPreference = value;
    }
}

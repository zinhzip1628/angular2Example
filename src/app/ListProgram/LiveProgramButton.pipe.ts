import {Pipe, PipeTransform, OnInit} from '@angular/core';
import {ILiveProgramList} from './ILiveProgramList';

@Pipe({
    name: 'LiveButtonPipe'
})
export class LiveProgramButton implements PipeTransform {

    str = '';
    transform(value: ILiveProgramList, LiveProgram: ILiveProgramList): string {
        this.str = '';
        if (LiveProgram.ProgamStatus === 1 || LiveProgram.ProgamStatus === 2) {
                return '';
            }
        this.str =
        '<button type="button" id="btnKichHoat" class="waves-effect waves-light btn btn-info btn-xs"' +
        'value="" + emp.id + "" title="Cập nhật"><i class="icon-edit"></i></button>&nbsp&nbsp' +
        '<button type="button" id="btnHuyCT" class="waves-effect waves-light btn btn-warning btn-danger btn-xs"' +
        'value="" + emp.id + "" title="Hủy CT" data-toggle="modal" href="#CancelProgramReason">' +
        '<i class="icon-remove-sign"></i></button>&nbsp&nbsp';
        // chương trình đã hoàn thành, hủy
        if (LiveProgram.ProgamStatus === 1 || LiveProgram.ProgamStatus === 2) {
            return this.str +=
            '<button type="button" id="btnShowNV" class="waves-effect waves-light btn btn-warning btn-xs"' +
            'value="' + LiveProgram.id + '" title="Hiển thị nhân viên thực hiện" data-toggle="modal"' +
            'href="#modalShowNV"><i class="icon-group"></i></button>&nbsp&nbsp';
        }

        // chưa duyệt chương trình / không duyệt CT / đã gửi mail
        if (LiveProgram.Approve === 0 || LiveProgram.Approve === 2 || LiveProgram.Approve === 3) {
            return this.str +=
            '<button type="button" id="btnMail" class="waves-effect waves-light btn btn-success btn-xs" ' +
            'value="' + LiveProgram.id + '" title="Sendmail"><i class="icon-archive"></i></button>&nbsp&nbsp';
        }

        // chưa tạo phiếu khảo sát
        if (LiveProgram.ResultSurvey === 0) {
            return this.str +=
                '<button type="button" id="btnSurvey" class="waves-effect waves-light btn btn-warning btn-xs" ' +
                'value="' + LiveProgram.id + '" title="Tạo phiếu khảo sát"><i class="icon-plus-sign-alt"></i></button>&nbsp&nbsp';
        }
        if (LiveProgram.SurveyStatus1 > 3 || LiveProgram.SurveyStatus2 > 3) {
            this.str +=
            '<button type="button" id="btnUpdateSurvey" class="waves-effect waves-light btn btn-success btn-xs" value="'
            + LiveProgram.id + '" title="Cập nhật phương án xử lý" data-toggle="modal" href="#ChangeUpdateSurvey">' +
            '<i class="icon-refresh"></i></button>&nbsp&nbsp';
        }
        // khi đã khảo sát thành công
        this.str += '<button type="button" id="btnShowNV" class="waves-effect waves-light btn btn-warning btn-xs"' +
        ' value="" + emp.id + "" title="Hiển thị nhân viên thực hiện" data-toggle="modal" href="#modalShowNV">' +
        '<i class="icon-group"></i></button>&nbsp&nbsp';
        this.str +=
        '<button type="button" id="btnShowSurvey" class="waves-effect waves-light btn btn-warning btn-xs"' +
        ' value="' + LiveProgram.id + '" title="Xem phiếu khảo sát"><i class="icon-file"></i></button>&nbsp&nbsp';
        // test
        // str += "<button type="button" id="btnNghiemThu" class="waves-effect waves-light btn btn-primary btn-xs"
        // value="" + emp.id + "" title="Nghiệm thu"><i class="icon-foursquare"></i></button>&nbsp&nbsp";

        // chưa nghiệm thu (Chưa nghiệm thu || Không nghiệm thu)
        if ((LiveProgram.Accept === 0 && LiveProgram.ConstructionStatus1 === 2 && LiveProgram.ConstructionStatus2 === 1
                && LiveProgram.TicketStatus === 1 && LiveProgram.SurveyStatus1 === 2 && LiveProgram.SurveyStatus2 === 3)
            || (LiveProgram.Accept === 0 && LiveProgram.StatusSolution === 3
                    && LiveProgram.TicketStatus === 1 && LiveProgram.ConstructionStatus2 === 1)) {
            this.str += '<button type="button" id="btnNghiemThu" class="waves-effect waves-light btn btn-primary btn-xs"' +
                'value="" + emp.id + "" title="Nghiệm thu"><i class="icon-foursquare"></i></button>&nbsp&nbsp';
        } else if (LiveProgram.Accept === 1) {// đã nghiệm thu nhưng chưa chuyển qua cho marketing
            this.str += '<button type="button" id="btnMarketing" class="waves-effect waves-light btn btn-success btn-xs"' +
            ' value="" + emp.id + "" title="Marketing"><i class="icon-suitcase"></i></button>&nbsp&nbsp"' +
            '<button type="button" id="btnHoantat" class="waves-effect waves-light btn btn-info btn-xs" ' +
            'value="' + LiveProgram.id + '" title="Hoàn tất CT" data-toggle="modal" href="#CompletedProgram">' +
            '<i class="icon-save"></i></button>&nbsp&nbsp';
        }
        return this.str;
    }
}

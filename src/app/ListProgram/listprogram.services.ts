import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { ILiveProgramList } from './ILiveProgramList';
import { ILiveProgram } from './ILiveProgram';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/Observable/throw';

@Injectable()
export class ListProgramServices {
        constructor (private _http: Http) {}
        getLivePrograms(): Observable<ILiveProgramList[]> {
            return this._http.get('http://localhost:54208/api/live/GetAllLiveProgram')
            .map((response: Response) => <ILiveProgramList>response.json())
            .catch(this.handleError);
        }
        getLiveProgram(id: number): Observable<ILiveProgram> {
            return this._http.get('http://localhost:54208/api/live/' + id)
            .map((response: Response) => <ILiveProgram>response.json())
            .catch(this.handleError);
        }
        handleError(error: Response) {
            return Observable.throw(error);
        }
}

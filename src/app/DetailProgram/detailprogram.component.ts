import {Component, OnInit} from '@angular/core';
import {ILiveProgramList} from '../ListProgram/ILiveProgramList';
import {ListProgramServices} from '../ListProgram/listprogram.services';
import { ILiveProgram } from '../ListProgram/ILiveProgram';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-detailprogram',
    templateUrl: '/detailprogram.component.html',
    styleUrls: ['/detailprogram.component.css']
})
export class DetailProgramComponent implements OnInit {
    LiveProgram: ILiveProgram;
    statusMessage = 'Loading data please wait ...';
    constructor ( private _ListProgramService: ListProgramServices, private _activatedRouter: ActivatedRoute) {
    }
    ngOnInit(): void {
        const empID = this._activatedRouter.snapshot.params['ID'];
        this._ListProgramService.getLiveProgram(empID).subscribe((liveprogramdata) => this.LiveProgram = liveprogramdata,
        (error => {
            this.statusMessage = 'Problem with the services. Please try again ...!';
        }));
    }
    onClickUpdate(): void {
        console.log(this.LiveProgram);
    }
    // set LiveProgram(value: ILiveProgram) {
    //     this.
    // }
}

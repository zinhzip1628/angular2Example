import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';
import { DetailProgramComponent } from './DetailProgram/detailprogram.component';
import { PageNotFoundComponent } from './PageNotFound/pagenotfound.component';

import { ListProgramComponent } from './ListProgram/listprogram.component';
import { ListProgramServices } from './ListProgram/listprogram.services';
import { LiveProgramState } from './ListProgram/LiveProgramState.pipe';
import { LiveProgramButton } from './ListProgram/LiveProgramButton.pipe';
import { UpdateProgramComponent } from './UpdateProgram/updateprogram.component';
import { TableComponent } from './TableTemplate/table.component';
import { GameMoKetComponent } from './GameMoKet/gamemoket.component';
import { ColourService } from './Colour/colour.service';

const appRoutes: Routes = [
  {path: 'listprogram', component: ListProgramComponent},
  {path: 'updateprogram', component: UpdateProgramComponent},
  {path: 'updateprogram/:ID', component: DetailProgramComponent},
  {path: 'gamemoket', component: GameMoKetComponent},
  {path: 'gamemoket/:ID', component: DetailProgramComponent},
  {path: '', redirectTo: '/listprogram', pathMatch: 'full'},
  {path: '**',  component: PageNotFoundComponent},
];

@NgModule({
  declarations: [
    AppComponent, DetailProgramComponent, ListProgramComponent, PageNotFoundComponent, LiveProgramState, LiveProgramButton
    , UpdateProgramComponent, TableComponent, GameMoKetComponent
  ],
  imports: [
    BrowserModule, HttpModule, FormsModule, RouterModule.forRoot(appRoutes, { useHash: true } )
  ],
  providers: [ListProgramServices,
    ColourService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
